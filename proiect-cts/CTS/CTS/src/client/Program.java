package client;

import models.UserOperations;

public class Program {

	public static void main(String[] args) {
		try {
			UserOperations operations = new UserOperations();
			operations.signUp();
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}
