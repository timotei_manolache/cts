package file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import models.User;

public class FileConnection {

	private List<User> users;
	private static FileConnection instance;

	private FileConnection() throws Exception {
		this.deserialize();
	}

	private void serialize() throws Exception {
		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream("Users.bin"));
			if(this.users.get(0) != null){
				out.writeObject(this.users.get(0));
			}
			
			out.close();
		} catch (FileNotFoundException ex) {
			File file = new File("Users.bin");
			file.createNewFile();
		}
	}

	@SuppressWarnings("unchecked")
	private void deserialize() throws Exception {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					"Users.bin"));
			this.users = new ArrayList<User>();
			this.users.add((User) in.readObject());
			in.close();
		} catch (FileNotFoundException ex) {
		}
	}

	public static FileConnection getInstance() throws Exception {
		if (instance == null) {
			instance = new FileConnection();
		}
		return instance;
	}

	public void addUser(User user) throws Exception {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}
		if (users != null) {
			this.users.add(user);
			this.serialize();
			this.deserialize();
		}
	}
	
	public List<User> getUsers() {
		return this.users;
	}
}
