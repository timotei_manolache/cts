package models;

import java.io.Serializable;

import database.DbConnection;

public class Address implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer streetNumber;
	private String street;
	private String city;
	private String county;
	private String country;
	private String zipCode;
	private DbConnection db;

	public Address() throws Exception {
		//this.db = DbConnection.getInstance();
	}

	public Address(Integer streetNumber, String street, String city, String county, String country) throws Exception {
		//this.db = DbConnection.getInstance();
		this.setStreetNumber(streetNumber);
		this.setStreet(street);
		this.setCity(city);
		this.setCounty(county);
		this.setCountry(country);
	}

	public Integer getStreetNumber() {
		return this.streetNumber;
	}

	public void setStreetNumber(Integer streetNumber) {
		if (streetNumber == null) {
			throw new IllegalArgumentException("Street number should not be null");
		} else if (streetNumber <= 0) {
			throw new IllegalArgumentException("Invalid street number!");
		}
		this.streetNumber = streetNumber;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		if (street == null) {
			throw new IllegalArgumentException("Street should not be null");
		} else if (street.length() < 5 || street.length() > 30) {
			throw new IllegalArgumentException("Street must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < street.length(); i++) {
				if (!(Character.isLetter(street.charAt(i))) && (street.charAt(i) != ' ')) {
					throw new IllegalArgumentException("Street must contain only letters and spaces");
				}
			}
		}
		this.street = street;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) throws Exception {
		if (city == null) {
			throw new IllegalArgumentException("City name should not be null");
		} else if (city.length() < 5 || city.length() > 30) {
			throw new IllegalArgumentException(
					"City name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < city.length(); i++) {
				if (!(Character.isLetter(city.charAt(i))) && (city.charAt(i) != ' ')) {
					throw new IllegalArgumentException("City name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < city.length(); i++) {
				String[] cityWords = city.split(" ");
				for (String word : cityWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the city name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j)) && !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the city name, except the first, must be lowercase");
						}
					}
				}
			}

			if (this.country.equals("Rom�nia")) {
				boolean isFound = false;
				for (County countyObj : db.getCounties()) {
					if (this.county.equals(countyObj.getName())) {
						for (String cityObj : db.getCitiesByCounty(countyObj)) {
							if (cityObj.equals(city)) {
								isFound = true;
							}
						}
					}
				}
				if (!isFound) {
					throw new IllegalArgumentException("City name not exists in database");
				}
			}
		}
		this.city = city;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) throws Exception {
		if (county == null) {
			throw new IllegalArgumentException("County name should not be null");
		} else if (county.length() < 3 || county.length() > 30) {
			throw new IllegalArgumentException(
					"County name must have at least 3 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < county.length(); i++) {
				if (!(Character.isLetter(county.charAt(i))) && (county.charAt(i) != ' ')) {
					throw new IllegalArgumentException("County name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < county.length(); i++) {
				String[] countyWords = county.split(" ");
				for (String word : countyWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the county name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j)) && !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the county name, except the first, must be lowercase");
						}
					}
				}
			}

			if (this.country.equals("Rom�nia")) {
				boolean isFound = false;
				for (County countyObj : db.getCounties()) {
					if (countyObj.getName().equals(county)) {
						isFound = true;
						break;
					}
				}
				if (!isFound) {
					throw new IllegalArgumentException("County name not exists in database");
				}
			}
		}
		this.county = county;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		if (country == null) {
			throw new IllegalArgumentException("Country name should not be null");
		} else if (country.length() < 5 || country.length() > 30) {
			throw new IllegalArgumentException(
					"Country name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < country.length(); i++) {
				if (!(Character.isLetter(country.charAt(i))) && (country.charAt(i) != ' ')) {
					throw new IllegalArgumentException("Country name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < country.length(); i++) {
				String[] countryWords = country.split(" ");
				for (String word : countryWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the country name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j)) && !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the country name, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.country = country;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		try {
			if (zipCode == null) {
				throw new IllegalArgumentException("Zip code should not be null");
			} else {
				Integer.parseInt(zipCode);
				if (zipCode.length() != 6) {
					throw new IllegalArgumentException("Zip code must contain 6 digits");
				} else {
					if (("" + zipCode.charAt(1)).equals("9")) {
						throw new IllegalArgumentException("The second of zip code should not be 9");
					}
				}
			}
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Zip code must contain only digits");
		}

		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "Address [streetNumber=" + streetNumber + ", street=" + street + ", city=" + city + ", county=" + county
				+ ", country=" + country + ", zipCode=" + zipCode + ", db=" + db + "]";
	}
}
