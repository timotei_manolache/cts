package models;

public class Grade {

	private String name;
	private Double value;
	private Integer credit;
	private Integer studyYear;

	public Grade() {

	}

	public Grade(String name, Double value, Integer credit, Integer studyYear) {
		this.setName(name);
		this.setValue(value);
		this.setCredit(credit);
		this.setStudyYear(studyYear);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Grade name should not be null");
		} else {
			if (name.length() < 3 || name.length() > 30) {
				throw new IllegalArgumentException(
						"Grade name must be between 1 and 10");
			}
		}
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		if (value == null) {
			throw new IllegalArgumentException("Grade value should not be null");
		} else {
			if (value <= 0 || value > 10) {
				throw new IllegalArgumentException(
						"Grade value must be between 1 and 10");
			}
		}
		this.value = value;
	}

	public Integer getCredit() {
		return this.credit;
	}

	public void setCredit(Integer credit) {
		if (credit == null) {
			throw new IllegalArgumentException(
					"Grade credit should not be null");
		} else {
			if (credit <= 0 || credit > 5) {
				throw new IllegalArgumentException(
						"Grade credit must be between 1 and 5");
			}
		}
		this.credit = credit;
	}

	public Integer getStudyYear() {
		return this.studyYear;
	}

	public void setStudyYear(Integer studyYear) {
		if (studyYear == null) {
			throw new IllegalArgumentException("Study year should not be null");
		} else {
			if (studyYear < 1 || studyYear > 5) {
				throw new IllegalArgumentException(
						"Study year must be between 1 and 5");
			}
		}
		this.studyYear = studyYear;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode() + this.getValue().hashCode()
				+ this.getCredit().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj instanceof Grade) {
			Grade g = (Grade) obj;
			return this.getName().equals(g.getName())
					&& this.getValue().equals(g.getValue())
					&& this.getCredit().equals(g.getValue());
		}
		return false;
	}
}
