package models;

public class AddressBuilder {

	private Address address;

	public AddressBuilder() throws Exception {
		this.address = new Address();
	}
	
	public AddressBuilder(Address address) {
		this.address = address;
	}
	
	public AddressBuilder setStreetNumber(Integer streetNumber) {
		this.address.setStreetNumber(streetNumber);
		return this;
	}

	public AddressBuilder setStreet(String street) {
		this.address.setStreet(street);
		return this;
	}


	public AddressBuilder setCity(String city) throws Exception {
		this.address.setCity(city);
		return this;
	}

	public AddressBuilder setCounty(String county) throws Exception {
		this.address.setCounty(county);
		return this;
	}
	
	public AddressBuilder setCountry(String country) {
		this.address.setCountry(country);
		return this;
	}

	public AddressBuilder setZipCode(String zipCode) {
		this.address.setZipCode(zipCode);
		return this;
	}
	
	public Address getAddress() {
		return this.address;
	}
}
