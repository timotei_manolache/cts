package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import database.DbConnection;

public class Student implements User {

	/**
	 * Attributes
	 * */
	private String firstName;
	private String lastName;
	private Date birthdate;
	private String placeOfBirth;
	private Address address;
	private String phone;
	private String email;
	private String university;
	private String specialization;
	private Integer studyYear;
	private Integer group;
	private String series;
	private List<Grade> grades;
	private String username;
	private String password;
	private DbConnection db;

	public Student() throws Exception {
		//this.db = DbConnection.getInstance();
		this.grades = new ArrayList<>();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (lastName == null) {
			throw new IllegalArgumentException("Last name should not be null");
		} else if (lastName.length() < 3 || lastName.length() > 30) {
			throw new IllegalArgumentException(
					"Last name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < lastName.length(); i++) {
				if (!(Character.isLetter(lastName.charAt(i)))
						&& (lastName.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"Last name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < lastName.length(); i++) {
				String[] lastNameWords = lastName.split(" ");
				for (String word : lastNameWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the last name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the last name, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		if (firstName == null) {
			throw new IllegalArgumentException("First name should not be null");
		} else if (firstName.length() < 3 || firstName.length() > 30) {
			throw new IllegalArgumentException(
					"First name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < firstName.length(); i++) {
				if (!(Character.isLetter(firstName.charAt(i)))
						&& (firstName.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"First name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < firstName.length(); i++) {
				String[] firstNameWords = firstName.split(" ");
				for (String word : firstNameWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the first name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the first name, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.firstName = firstName;
	}

	public String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) throws Exception {
		if (placeOfBirth == null) {
			throw new IllegalArgumentException(
					"Place of birth should not be null");
		} else if (placeOfBirth.length() < 3 || placeOfBirth.length() > 30) {
			throw new IllegalArgumentException(
					"Place of birth must have at least 3 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < placeOfBirth.length(); i++) {
				if (!(Character.isLetter(placeOfBirth.charAt(i)))
						&& (placeOfBirth.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"Place of birth must contain only letters and spaces");
				}
			}
			for (int i = 0; i < placeOfBirth.length(); i++) {
				String[] placeOfBirthWords = placeOfBirth.split(" ");
				for (String word : placeOfBirthWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the place of birth must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the place of birth, except the first, must be lowercase");
						}
					}
				}
			}
			boolean isFound = false;
			for (String city : this.db.getCities()) {
				if (placeOfBirth.equals(city)) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"Place of birth does not exists in database");
			}
		}
		this.placeOfBirth = placeOfBirth;
	}

	public Integer getStreetNumber() {
		return this.address.getStreetNumber();
	}

	public void setStreetNumber(Integer streetNumber) {
		this.address.setStreetNumber(streetNumber);
	}

	public String getStreet() {
		return this.address.getStreet();
	}

	public void setStreet(String street) {
		this.address.setStreet(street);
	}

	public String getCity() {
		return this.address.getCity();
	}

	public void setCity(String city) throws Exception {
		this.address.setCity(city);
	}

	public String getCounty() {
		return this.address.getCounty();
	}

	public void setCounty(String county) throws Exception {
		this.address.setCounty(county);
	}

	public String getCountry() {
		return this.address.getCountry();
	}

	public void setCountry(String country) {
		this.address.setCountry(country);
	}

	public String getZipCode() {
		return this.address.getZipCode();
	}

	public void setZipCode(String zipCode) {
		this.address.setZipCode(zipCode);
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		if (phone == null) {
			throw new IllegalArgumentException(
					"Phone number should not be null");
		} else if (phone.length() != 10) {
			throw new IllegalArgumentException(
					"Phone number must have 10 digits");
		} else {
			try {
				Long.parseLong(phone);
			} catch (NumberFormatException ex) {
				throw new IllegalArgumentException(
						"Phone number must have only digits");
			}
		}
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		if (email == null) {
			throw new IllegalArgumentException(
					"Email address should not be null");
		} else {
			Pattern emailPattern = Pattern.compile(
					"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = emailPattern.matcher(email);
			if (!matcher.find()) {
				throw new IllegalArgumentException(
						"The email address is not valid");
			}
		}
		this.email = email;
	}

	public String getUniversity() {
		return this.university;
	}

	public void setUniversity(String university) {
		if (university == null) {
			throw new IllegalArgumentException(
					"University name should not be null");
		} else if (university.length() < 3 || university.length() > 30) {
			throw new IllegalArgumentException(
					"University name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < university.length(); i++) {
				if (!(Character.isLetter(university.charAt(i)))
						&& (university.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"University name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < university.length(); i++) {
				String[] universityWords = university.split(" ");
				for (String word : universityWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the university must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the university, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.university = university;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		if (specialization == null) {
			throw new IllegalArgumentException(
					"Specialization should not be null");
		} else if (specialization.length() < 3 || specialization.length() > 30) {
			throw new IllegalArgumentException(
					"Specialization must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < specialization.length(); i++) {
				if (!(Character.isLetter(specialization.charAt(i)))
						&& (specialization.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"Specialization must contain only letters and spaces");
				}
			}
		}
		this.specialization = specialization;
	}

	public Integer getStudyYear() {
		return this.studyYear;
	}

	public void setStudyYear(Integer studyYear) {
		if (studyYear == null) {
			throw new IllegalArgumentException("Study year should not be null");
		} else {
			if (studyYear < 1 || studyYear > 5) {
				throw new IllegalArgumentException(
						"Study year must be between 1 and 5");
			}
		}
		this.studyYear = studyYear;
	}

	public Integer getGroup() {
		return this.group;
	}

	public void setGroup(Integer group) {
		if (group == null) {
			throw new IllegalArgumentException("Group should not be null");
		} else {
			if (group < 0) {
				throw new IllegalArgumentException("Invalid group number");
			}
		}
		this.group = group;
	}

	public String getSeries() {
		return this.series;
	}

	public void setSeries(String series) {
		if (series == null) {
			throw new IllegalArgumentException("Series should not be null");
		}
		this.series = series;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	private long daysBetween(Calendar startDate, Calendar endDate) {
		Calendar date = (Calendar) startDate.clone();
		long daysBetween = 0;
		while (date.before(endDate)) {
			date.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}

	public void setBirthdate(Date birthdate) {
		if (birthdate == null) {
			throw new IllegalArgumentException("Birthdate should not be null");
		} else {
			Calendar minDate = Calendar.getInstance();
			minDate.set(1970, 01, 01);
			Calendar birthDate = Calendar.getInstance();
			birthDate.setTime(birthdate);
			Calendar currentDate = Calendar.getInstance();
			currentDate.setTime(new Date());

			if (daysBetween(minDate, birthDate) == 0) {
				throw new IllegalArgumentException(
						"Birthdate must be higher than 01.01.1970");
			} else if ((float) daysBetween(birthDate, currentDate) / 365 < 18) {
				throw new IllegalArgumentException(
						"A student must have at least 18 years");
			}
		}
		this.birthdate = birthdate;
	}

	public void displayGrades() {
		for (Grade grade : this.grades) {
			System.out.println(grade);
		}
	}

	public void addGrade(Grade grade) {
		this.grades.add(grade);
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public void setUsername(String username) {
		if (username == null) {
			throw new IllegalArgumentException("Username should not be null");
		} else if (username.length() < 5 || username.length() > 30) {
			throw new IllegalArgumentException(
					"Username must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < username.length(); i++) {
				if (!(Character.isLetter(username.charAt(i)))
						&& !(Character.isDigit(username.charAt(i)))
						&& username.charAt(i) != '.') {
					throw new IllegalArgumentException(
							"Username must contain only letters, dots and numbers");
				}
			}
			for (int i = 0; i < username.length(); i++) {
				if (Character.isLetter(username.charAt(i))
						&& !(Character.isLowerCase(username.charAt(i)))) {
					throw new IllegalArgumentException(
							"Username must be lowercase");
				}
			}
		}
		this.username = username;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(String password) {
		if (password == null) {
			throw new IllegalArgumentException("Password should not be null");
		} else if (password.length() < 5 || password.length() > 30) {
			throw new IllegalArgumentException(
					"Password must have at least 5 characters and a maximum of 30 characters");
		} else {
			if (!Character.isLetter(password.charAt(0))) {
				throw new IllegalArgumentException(
						"Password must begin with a letter");
			}
			boolean isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isDigit(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A digit must occur at least once in the password");
			}
			isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isLowerCase(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A lower case letter must occur at least once in the password");
			}
			isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isUpperCase(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A upper case letter must occur at least once in the password");
			}
			isFound = false;
			char[] specialCharacters = { '@', '#', '$', '%', '^', '&', '+', '=' };
			for (int i = 0; i < password.length(); i++) {
				for (int j = 0; j < specialCharacters.length; j++) {
					if (password.charAt(i) == specialCharacters[j]) {
						isFound = true;
					}
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A a special character must occur at least once in the password");
			}
			for (int i = 0; i < password.length(); i++) {
				if (Character.isSpaceChar(password.charAt(i))) {
					throw new IllegalArgumentException(
							"No whitespace allowed in the entire password");
				}
			}
		}
		this.password = password;
	}

	public List<Grade> getGrades() {
		return this.grades;
	}

	public Double getAverageOfGradeByStudyYear(Integer studyYear) {
		double average = 0;
		int credits = 0;
		for (Grade grade : this.grades) {
			if (grade.getStudyYear().equals(studyYear)) {
				average += grade.getValue() * grade.getCredit();
				credits += grade.getCredit();
			}
		}
		return (double)average / credits;
	}

	@Override
	public String toString() {
		return "Student: " + this.getFirstName();
	}
}
