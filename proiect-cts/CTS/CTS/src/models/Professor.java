package models;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Professor implements User, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Attributes
	 * */
	private String firstName;
	private String lastName;
	private Address address;
	private String university;
	private Double salary;
	private Map<Integer, List<Student>> groups;
	private String username;
	private String password;

	/**
	 * Constructor
	 * */
	public Professor() throws Exception {
		this.groups = new HashMap<>(0);
		this.address = new Address();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (lastName == null) {
			throw new IllegalArgumentException("Last name should not be null");
		} else if (lastName.length() < 3 || lastName.length() > 30) {
			throw new IllegalArgumentException(
					"Last name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < lastName.length(); i++) {
				if (!(Character.isLetter(lastName.charAt(i)))
						&& (lastName.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"Last name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < lastName.length(); i++) {
				String[] lastNameWords = lastName.split(" ");
				for (String word : lastNameWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the last name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the last name, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		if (firstName == null) {
			throw new IllegalArgumentException("First name should not be null");
		} else if (firstName.length() < 3 || firstName.length() > 30) {
			throw new IllegalArgumentException(
					"First name must have at least 3 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < firstName.length(); i++) {
				if (!(Character.isLetter(firstName.charAt(i)))
						&& (firstName.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"First name must contain only letters and spaces");
				}
			}
			for (int i = 0; i < firstName.length(); i++) {
				String[] firstNameWords = firstName.split(" ");
				for (String word : firstNameWords) {
					if (Character.isLowerCase(word.charAt(0))) {
						throw new IllegalArgumentException(
								"First letter of every word of the first name must be uppercase");
					}
					for (int j = 1; j < word.length(); j++) {
						if (Character.isLetter(word.charAt(j))
								&& !(Character.isLowerCase(word.charAt(j)))) {
							throw new IllegalArgumentException(
									"Every letter in every word of the first name, except the first, must be lowercase");
						}
					}
				}
			}
		}
		this.firstName = firstName;
	}

	public Integer getStreetNumber() {
		return this.address.getStreetNumber();
	}

	public void setStreetNumber(Integer streetNumber) {
		this.address.setStreetNumber(streetNumber);
	}

	public String getStreet() {
		return this.address.getStreet();
	}

	public void setStreet(String street) {
		this.address.setStreet(street);
	}

	public String getCity() {
		return this.address.getCity();
	}

	public void setCity(String city) throws Exception {
		this.address.setCity(city);
	}

	public String getCounty() {
		return this.address.getCounty();
	}

	public void setCounty(String county) throws Exception {
		this.address.setCounty(county);
	}

	public String getCountry() {
		return this.address.getCountry();
	}

	public void setCountry(String country) {
		this.address.setCountry(country);
	}

	public String getZipCode() {
		return this.address.getZipCode();
	}

	public void setZipCode(String zipCode) {
		this.address.setZipCode(zipCode);
	}

	public String getUniversity() {
		return this.university;
	}

	public void setUniversity(String university) {
		if (university == null) {
			throw new IllegalArgumentException(
					"University name should not be null");
		} else if (university.length() < 3 || university.length() > 30) {
			throw new IllegalArgumentException(
					"University name must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < university.length(); i++) {
				if (!(Character.isLetter(university.charAt(i)))
						&& (university.charAt(i) != ' ')) {
					throw new IllegalArgumentException(
							"University name must contain only letters and spaces");
				}
			}
		}
		this.university = university;
	}

	public Double getSalary() {
		return this.salary;
	}

	public void setSalary(Double salary) {
		if (salary == null) {
			throw new IllegalArgumentException("Salary should not be null");
		} else {
			if (salary.doubleValue() <= 0 || salary.doubleValue() > 10000) {
				throw new IllegalArgumentException(
						"Salary must be between 0 and 10000");
			}
		}
		this.salary = salary;
	}

	public void addStudent(Integer group, Student stud) {
		if (group >= 1000 && group < 2000) {
			List<Student> students = this.groups.get(group);
			if (students == null) {
				students = new ArrayList<>();
			}
			students.add(stud);
			this.groups.put(group, students);
		}
	}

	public List<Student> getStudentsByGroup(Integer group) {
		if (group >= 1000 && group < 2000) {
			return this.groups.get(group);
		}
		return null;
	}

	public void displayStudentsByGroup() {
		for (Entry<Integer, List<Student>> entry : this.groups.entrySet()) {
			System.out.println("Group: " + entry.getKey());
			for (Student stud : entry.getValue()) {
				System.out.println("   " + stud);
			}
		}
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public void setUsername(String username) {
		if (username == null) {
			throw new IllegalArgumentException("Username should not be null");
		} else if (username.length() < 5 || username.length() > 30) {
			throw new IllegalArgumentException(
					"Username must have at least 5 characters and a maximum of 30 characters");
		} else {
			for (int i = 0; i < username.length(); i++) {
				if (!(Character.isLetter(username.charAt(i)))
						&& !(Character.isDigit(username.charAt(i)))
						&& username.charAt(i) != '.') {
					throw new IllegalArgumentException(
							"Username must contain only letters, dots and numbers");
				}
			}
			for (int i = 0; i < username.length(); i++) {
				if (Character.isLetter(username.charAt(i))
						&& !(Character.isLowerCase(username.charAt(i)))) {
					throw new IllegalArgumentException(
							"Username must be lowercase");
				}
			}
		}
		this.username = username;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(String password) {
		if (password == null) {
			throw new IllegalArgumentException("Password should not be null");
		} else if (password.length() < 5 || password.length() > 30) {
			throw new IllegalArgumentException(
					"Password must have at least 5 characters and a maximum of 30 characters");
		} else {
			if (!Character.isLetter(password.charAt(0))) {
				throw new IllegalArgumentException(
						"Password must begin with a letter");
			}
			boolean isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isDigit(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A digit must occur at least once in the password");
			}
			isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isLowerCase(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A lower case letter must occur at least once in the password");
			}
			isFound = false;
			for (int i = 0; i < password.length(); i++) {
				if (Character.isUpperCase(password.charAt(i))) {
					isFound = true;
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A upper case letter must occur at least once in the password");
			}
			isFound = false;
			char[] specialCharacters = { '@', '#', '$', '%', '^', '&', '+', '=', '-' };
			for (int i = 0; i < password.length(); i++) {
				for (int j = 0; j < specialCharacters.length; j++) {
					if (password.charAt(i) == specialCharacters[j]) {
						isFound = true;
					}
				}
			}
			if (!isFound) {
				throw new IllegalArgumentException(
						"A a special character must occur at least once in the password");
			}
			for (int i = 0; i < password.length(); i++) {
				if (Character.isSpaceChar(password.charAt(i))) {
					throw new IllegalArgumentException(
							"No whitespace allowed in the entire password");
				}
			}
		}
		this.password = password;
	}

	@Override
	public String toString() {
		return "Professor [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", university="
				+ university + ", salary=" + salary + ", groups=" + groups + ", username=" + username + ", password="
				+ password + "]";
	}
}
