package models;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Date;

import file.FileConnection;

public class UserOperations {

	private FileConnection fileManager;

	public UserOperations() throws Exception {
		this.fileManager = FileConnection.getInstance();
	}

	public void signUp() throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("-----SIGN UP USER-----");
		System.out.print("User type (p/s): ");
		String choice = br.readLine();
		if (choice.equalsIgnoreCase("p")) {
			Professor user = (Professor) UserFactory
					.createUser(UserType.PROFESSOR);
			System.out.print("First name: ");
			user.setFirstName(br.readLine());
			System.out.print("Last name: ");
			user.setLastName(br.readLine());
			System.out.print("Country: ");
			user.setCountry(br.readLine());
			System.out.print("County: ");
			user.setCounty(br.readLine());
			System.out.print("City: ");
			user.setCity(br.readLine());
			System.out.print("Street: ");
			user.setStreet(br.readLine());
			System.out.print("Street nb: ");
			user.setStreetNumber(Integer.parseInt(br.readLine()));
			System.out.print("Zip code: ");
			user.setZipCode(br.readLine());
			System.out.print("University: ");
			user.setUniversity(br.readLine());
			System.out.println("Salary: ");
			user.setSalary(Double.parseDouble(br.readLine()));
			System.out.print("Username: ");
			user.setUsername(br.readLine());
			System.out.print("Password: ");
			user.setPassword(br.readLine());
			this.fileManager.addUser(user);
		} else if (choice.equalsIgnoreCase("s")) {
			Student user = (Student) UserFactory.createUser(UserType.STUDENT);
			System.out.println("First name: ");
			user.setFirstName(br.readLine());
			System.out.println("Last name: ");
			user.setLastName(br.readLine());
			System.out.println("Place of birth: ");
			user.setPlaceOfBirth(br.readLine());
			System.out.print("Country: ");
			user.setCountry(br.readLine());
			System.out.print("County: ");
			user.setCounty(br.readLine());
			System.out.print("City: ");
			user.setCity(br.readLine());
			System.out.print("Street: ");
			user.setStreet(br.readLine());
			System.out.print("Street nb: ");
			user.setStreetNumber(Integer.parseInt(br.readLine()));
			System.out.print("Zip code: ");
			user.setZipCode(br.readLine());
			System.out.print("University: ");
			user.setUniversity(br.readLine());
			System.out.println("Specialization: ");
			user.setSpecialization(br.readLine());
			System.out.println("Study year: ");
			user.setStudyYear(Integer.parseInt(br.readLine()));
			System.out.println("Group: ");
			user.setGroup(Integer.parseInt(br.readLine()));
			System.out.println("Series: ");
			user.setSeries(br.readLine());
			System.out.print("Username: ");
			user.setUsername(br.readLine());
			System.out.print("Password: ");
			user.setPassword(br.readLine());
			this.fileManager.addUser(user);
		} else {
			System.out.println("Wrong choice");
			return;
		}
		br.close();
	}

}
