package models;

public class UserFactory {

	public static User createUser(UserType type) throws Exception {
		switch (type) {
			case PROFESSOR: return new Professor();
			case STUDENT: return new Student();
			default: return null;
		}
	}

}
