package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.County;
import models.User;

public class DbConnection {

	/**
	 * Attributes
	 * */
	private Connection connection;
	private PreparedStatement statement;
	private ResultSet resultSet;
	private static DbConnection instance;

	/**
	 * Private constructor to create singleton class
	 * */
//	private DbConnection() throws Exception {
//		Class.forName("com.mysql.jdbc.Driver");
//		connection = DriverManager.getConnection("jdbc:mysql://localhost/cts?"
//				+ "user=root&password=");
//	}

	/**
	 * Singleton acces method
	 * */
//	public static DbConnection getInstance() throws Exception {
//		if (instance == null) {
//			instance = new DbConnection();
//		}
//		return instance;
//	}

	/**
	 * Get all counties name from database
	 * */
	public List<County> getCounties() throws Exception {
		List<County> counties = new ArrayList<>();
		counties.add(new County(1, "Satu Mare"));
		counties.add(new County(2, "Arad"));
		counties.add(new County(3, "Sibiu"));
		counties.add(new County(4, "Covasna"));
		counties.add(new County(5, "Tulcea"));
//		statement = connection.prepareStatement("select * from judete");
//		resultSet = statement.executeQuery();
//		while (resultSet.next()) {
//			counties.add(new County(resultSet.getInt("id"), resultSet.getString("name")));
//		}
//		resultSet.close();
//		statement.close();
		return counties;
	}

	/**
	 * Get all names of cities by county name
	 * */
	public List<String> getCitiesByCounty(County county) throws Exception {
		List<String> cities = new ArrayList<>();
//		statement = connection.prepareStatement("select name from localitati where county_id = " + county.getId());
//		resultSet = statement.executeQuery();
//		while (resultSet.next()) {
//			cities.add(resultSet.getString("name"));
//		}
//		resultSet.close();
//		statement.close();
		
		if (county.getName() == "Satu Mare") {
			cities.add("Moreni");
			cities.add("Pucioasa");
		}
		if (county.getName() == "Arad") {
			cities.add("Glodeni");
			cities.add("Bucuresti");
		}
		if (county.getName() == "Sibiu") {
			cities.add("Timisoara");
			cities.add("Constanta");
		}
		if (county.getName() == "Covasna") {
			cities.add("Bacau");
			cities.add("Giurciu");
		}
		if (county.getName() == "Tulcea") {
			cities.add("Cluj");
			cities.add("Iasi");
		}
		
		return cities;
	}
	
	/**
	 * Get all city names
	 * */
	public List<String> getCities() throws Exception {
		List<String> cities = new ArrayList<>();
		cities.add("Moreni");
		cities.add("Pucioasa");
		cities.add("Glodeni");
		cities.add("Bucuresti");
		cities.add("Timisoara");
		cities.add("Constanta");
		cities.add("Bacau");
		cities.add("Giurciu");
		cities.add("Cluj");
		cities.add("Iasi");
//		statement = connection.prepareStatement("select name from localitati");
//		resultSet = statement.executeQuery();
//		while (resultSet.next()) {
//			cities.add(resultSet.getString("name"));
//		}
//		resultSet.close();
//		statement.close();
		return cities;
	}
}
