package tests;

import junit.framework.TestCase;
import models.Professor;
import models.UserFactory;
import models.UserType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FirstNameTests extends TestCase {

	private Professor prof;

	public FirstNameTests(String name) {
		super(name);
	}
	
	@Before
	public void setUp() {
		try {
			this.prof = (Professor) UserFactory.createUser(UserType.PROFESSOR);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testFirstNameCorrect() {
		try {
			this.prof.setFirstName("Manolache");
			assertEquals("Manolache", this.prof.getFirstName());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testFirstNameWrong1() {
		try {
			this.prof.setFirstName(null);
			assertNull(this.prof.getFirstName());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertNull(this.prof.getFirstName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testFirstNameWrong2() {
		try {
			this.prof.setFirstName("Ma");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Ma", this.prof.getFirstName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testFirstNameWrong3() {
		try {
			this.prof.setFirstName("ManolacheManolacheManolache");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("ManolacheManolacheManolache",
					this.prof.getFirstName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testFirstNameWrong4() {
		try {
			this.prof.setFirstName("Manolache123");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Manolache123", this.prof.getFirstName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testFirstNameWrong5() {
		try {
			this.prof = (Professor) UserFactory.createUser(UserType.PROFESSOR);
			this.prof.setFirstName("manolache");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("manolache", this.prof.getFirstName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testFirstNameWrong6() {
		try {
			this.prof.setFirstName("Manol ache");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Manol ache", this.prof.getFirstName());
		}
	}

	@After
	public void tearDown() throws Exception {
		this.prof = null;
	}
}
