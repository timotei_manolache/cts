package tests;

import java.io.BufferedReader;
import java.io.FileReader;
import junit.framework.TestCase;
import models.AddressBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CityAddressTests extends TestCase {

	private AddressBuilder addressBuilder;
	private BufferedReader reader1;
	private BufferedReader reader2;

	public CityAddressTests(String name) {
		super(name);
	}

	@Before
	public void setUp() {
		try {
			this.addressBuilder = new AddressBuilder();
			this.reader1 = new BufferedReader(new FileReader(
					"CitiesCorrect.txt"));
			this.reader2 = new BufferedReader(new FileReader("CitiesWrong.txt"));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testCityAddressCorrect() {
		try {
			this.addressBuilder.setCountry("România").setCounty("Sector6")
					.setCity("Bucuresti");
			assertEquals("Sector6", this.addressBuilder.getAddress()
					.getCity());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			fail(ex.toString());
		}
	}

	@Test
	public void testCitiesAddressCorrectUsingFile() {
		try {
			String line = reader1.readLine();
			while (line != null) {
				this.addressBuilder.setCountry("România")
						.setCounty("Sector6").setCity(line);
				System.out.print(this.addressBuilder.getAddress().getCity()
						+ " ");
				assertEquals(line, this.addressBuilder.getAddress().getCity());
				line = reader1.readLine();
			}
			System.out.println();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCitiesAddressWrongUsingFile() {
		String line = null;
		try {
			line = reader2.readLine();
			while (line != null) {
				try {
					this.addressBuilder.setCountry("România")
							.setCountry("Sector6").setCity(line);
					assertEquals(line, this.addressBuilder.getAddress()
							.getCity());
					System.out.print(this.addressBuilder.getAddress().getCity()
							+ " ");
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
					assertEquals(line, this.addressBuilder.getAddress()
							.getCity());
				}
				line = reader2.readLine();
			}
			System.out.println();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testCityAddressWrong1() {
		try {
			this.addressBuilder.setCity(null);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertNull(this.addressBuilder.getAddress().getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong2() {
		try {
			this.addressBuilder.setCity("Buc");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Tar", this.addressBuilder.getAddress().getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong3() {
		try {
			this.addressBuilder
					.setCity("Bucurestiiiiii");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Bucurestiiiiii",
					this.addressBuilder.getAddress().getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong4() {
		try {
			this.addressBuilder.setCity("Bucur3sty");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			fail(ex.toString());
			assertEquals("Bucur3sty", this.addressBuilder.getAddress()
					.getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong5() {
		try {
			this.addressBuilder.setCity("bucuresti");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			fail(ex.toString());
			assertEquals("bucuresti", this.addressBuilder.getAddress()
					.getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong6() {
		try {
			this.addressBuilder.setCity("BucUrEsti");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			fail(ex.toString());
			assertEquals("BucUrEsti", this.addressBuilder.getAddress()
					.getCity());
		}
	}

	@Test(expected = AssertionError.class)
	public void testCityAddressWrong7() {
		try {
			this.addressBuilder.setCountry("România").setCounty("Sector6")
					.setCity("Random");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			fail(ex.toString());
			assertEquals("Random", this.addressBuilder.getAddress().getCity());
		}
	}

	@After
	public void tearDown() throws Exception {
		this.reader1.close();
		this.reader2.close();
		this.addressBuilder = null;
	}
}
