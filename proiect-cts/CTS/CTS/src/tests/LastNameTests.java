package tests;

import junit.framework.TestCase;
import models.Student;
import models.UserFactory;
import models.UserType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LastNameTests extends TestCase {

	private Student stud;

	public LastNameTests(String name) {
		super(name);
	}
	
	@Before
	public void setUp() {
		try {
			this.stud = (Student) UserFactory.createUser(UserType.STUDENT);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testLastNameCorrect() {
		try {
			this.stud.setLastName("Timo");
			assertEquals("Timo", this.stud.getLastName());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Test
	public void testLastNameWrong1() {
		try {
			this.stud.setLastName(null);
			assertNull(this.stud.getLastName());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertNull(this.stud.getLastName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testLastNameWrong2() {
		try {
			this.stud.setLastName("Ti");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Ti", this.stud.getLastName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testLastNameWrong3() {
		try {
			this.stud.setLastName("Timotimotimotimotimotimotimo");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Timotimotimotimotimotimotimo",
					this.stud.getLastName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testLastNameWrong4() {
		try {
			this.stud.setLastName("Timo123");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Timo123", this.stud.getLastName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testLastNameWrong5() {
		try {
			this.stud.setLastName("timo");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("timo", this.stud.getLastName());
		}
	}

	@Test(expected = AssertionError.class)
	public void testLastNameWrong6() {
		try {
			this.stud.setLastName("Timotei");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			assertEquals("Timotei", this.stud.getLastName());
		}
	}

	@After
	public void tearDown() {
		this.stud = null;
	}
}
